Votre inscription a été désactivée de la liste de diffusion $listname parce qu'elle
a reçu un nombre de rebonds indiquant qu'il y a probablement un problème de
distribution de messages à $sender_email. Vous devriez voir avec votre
administrateur de mails pour plus d'aide.

Si vous avez la moindre question ou le moindre problème, vous pouvez contacter
le propriétaire de la liste à l'adresse :

    $owner_email
